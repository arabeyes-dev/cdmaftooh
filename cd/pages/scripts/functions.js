/*
	Author: djihed@gmail.com
	Version: 1.0
	Date: 14/08/2005
*/


function showcaption(number)
{
		var looper;
		looper=0;
		while (looper <= 23 )
		{
			if(looper != number)
			{
				document.getElementById(looper).setAttribute("style", "border-top-color: #AAAABB; border-style: solid;");
			}
			looper++;
		}
		document.getElementById("caption").setAttribute("style", "border-width: 1px 1px 0px 1px; border-style: solid; border-color: #AAAABB;  background-color: lightcyan;");
		updatecaption(number);
}

function hidecaption(number)
{
		var looper = 0;
		while (looper <= 23 )
		{
			if(looper != number)
			{
				document.getElementById(looper).setAttribute("style", "");
			}
			looper++;
		}
		document.getElementById("caption").setAttribute("style", "");
		document.getElementById("caption").innerHTML = "";
}

function updatecaption(number)
{
	switch(number)
	{
		case 0: document.getElementById("caption").innerHTML = "Firefox";
		break;
		case 1: document.getElementById("caption").innerHTML = "Thunderbird";
		break;
		case 2: document.getElementById("caption").innerHTML = "OpenOffice.Org";
		break;
		case 3: document.getElementById("caption").innerHTML = "Eclipse";
		break;
		case 4: document.getElementById("caption").innerHTML = "PHP";
		break;
		case 5: document.getElementById("caption").innerHTML = "MySql";
		break;
		case 6: document.getElementById("caption").innerHTML = "Apache";
		break;
		case 7: document.getElementById("caption").innerHTML = "AbiWord";
		break;
		case 8: document.getElementById("caption").innerHTML = "Dev-C++";
		break;
		case 9: document.getElementById("caption").innerHTML = "Dev-PHP";
		break;
		case 10: document.getElementById("caption").innerHTML = "Blender 3D";
		break;
		case 11: document.getElementById("caption").innerHTML = "7zip";
		break;
		case 12: document.getElementById("caption").innerHTML = "GTK+";
		break;
		case 13: document.getElementById("caption").innerHTML = "Audacity";
		break;
		case 14: document.getElementById("caption").innerHTML = "Media Player Classic";
		break;
		case 15: document.getElementById("caption").innerHTML = "FileZilla";
		break;
		case 16: document.getElementById("caption").innerHTML = "Gaim";
		break;
		case 17: document.getElementById("caption").innerHTML = "Httrack";
		break;
		case 18: document.getElementById("caption").innerHTML = "Gimp";
		break;
		case 19: document.getElementById("caption").innerHTML = "PDF Creator";
		break;
		case 20: document.getElementById("caption").innerHTML = "Tuxracer";
		break;
		case 21: document.getElementById("caption").innerHTML = "Dia";
		break;
		case 22: document.getElementById("caption").innerHTML = "VideoLAN";
		break;
		case 23: document.getElementById("caption").innerHTML = "Celestia";
		break;
	}
}


/*
Too much redundancy, know a better approach? please contact author
djihed@gmail.com
In reality, things were planned so that each software has a set of screenshots,
not only one, but with this new reduced version all of these files could be combined
in one manipulated by some javascript
 */
function screenShots(number)
{
	var filename = "screenshot.html";
	switch(number)
	{
		case 1: filename = "audacityscr.html";
		break;
		case 2: filename = "vlcscr.html";
		break;
		case 3: filename = "gimpscr.html";
		break;
		case 4: filename = "blenderscr.html";
		break;
		case 5: filename = "mpcscr.html";
		break;
		case 6: filename = "oooscr.html";
		break;
		case 7: filename = "abiwordscr.html";
		break;
		case 8: filename = "7zipscr.html";
		break;
		case 9: filename = "pdfcreatorscr.html";
		break;
		case 10: filename = "pdfcreatorscr.html";
		break;
		case 11: filename = "celestiascr.html";
		break;
		case 12: filename = "gnuplotscr.html";
		break;
		case 13: filename = "firefoxscr.html";
		break;
		case 14: filename = "thunderbirdscr.html";
		break;
		case 15: filename = "gaimscr.html";
		break;
		case 16: filename = "httrackscr.html";
		break;
		case 17: filename = "filezillascr.html";
		break;
		case 18: filename = "phpscr.html";
		break;
		case 19: filename = "phpscr.html";
		break;
		case 20: filename = "pythonscr.html";
		break;
		case 21: filename = "devcppscr.html";
		break;
		case 22: filename = "devphpscr.html";
		break;
		case 23: filename = "diascr.html";
		break;
		case 24: filename = "eclipsescr.html";
		break;
		case 25: filename = "notepad2scr.html";
		break;
		case 26: filename = "apachescr.html";
		break;
		case 27: filename = "mysqlscr.html";
		break;
		case 28: filename = "tuxracerscr.html";
		break;
		case 29: filename = "winboardscr.html";
		break;

	}
	window.open("scrfiles/"+filename,"Screenshots","height=500,width=660,left=50,top=50");

}

function showhelp(number)
{
	var filename = "screenshot.html";
	switch(number)
	{
		case 1: filename = "media/audacity.lch";
		break;
		case 2: filename = "vlc.html";
		break;
		case 3: filename = "gimp.html";
		break;
		case 4: filename = "blender.html";
		break;
		case 5: filename = "mpc.html";
		break;
		case 6: filename = "ooo.html";
		break;
		case 7: filename = "abiword.html";
		break;
		case 8: filename = "7zip.html";
		break;
		case 9: filename = "pdfcreator.html";
		break;
		case 10: filename = "pdfcreator.html";
		break;
		case 11: filename = "celestia.html";
		break;
		case 12: filename = "gnuplot.html";
		break;
		case 13: filename = "firefox.html";
		break;
		case 14: filename = "thunderbird.html";
		break;
		case 15: filename = "gaim.html";
		break;
		case 16: filename = "httrack.html";
		break;
		case 17: filename = "filezilla.html";
		break;
		case 18: filename = "gtk.html";
		break;
		case 19: filename = "php.html";
		break;
		case 20: filename = "python.html";
		break;
		case 21: filename = "devcpp.html";
		break;
		case 22: filename = "devphp.html";
		break;
		case 23: filename = "dia.html";
		break;
		case 24: filename = "eclipse.html";
		break;
		case 25: filename = "notepad2.html";
		break;
		case 26: filename = "apache.html";
		break;
		case 27: filename = "mysql.html";
		break;
		case 28: filename = "tuxracer.html";
		break;
		case 29: filename = "winboard.html";
		break;

	}
	window.open("../../links/"+filename);

}


function showhelp2(number)
{
	var filename = "screenshot.html";
	switch(number)
	{
		case 1: filename = "audacity.html";
		break;
		case 2: filename = "vlc.html";
		break;
		case 3: filename = "gimp.html";
		break;
		case 4: filename = "blender.html";
		break;
		case 5: filename = "mpc.html";
		break;
		case 6: filename = "ooo.html";
		break;
		case 7: filename = "abiword.html";
		break;
		case 8: filename = "7zip.html";
		break;
		case 9: filename = "pdfcreator.html";
		break;
		case 10: filename = "pdfcreator.html";
		break;
		case 11: filename = "celestia.html";
		break;
		case 12: filename = "gnuplot.html";
		break;
		case 13: filename = "firefox.html";
		break;
		case 14: filename = "thunderbird.html";
		break;
		case 15: filename = "gaim.html";
		break;
		case 16: filename = "httrack.html";
		break;
		case 17: filename = "filezilla.html";
		break;
		case 18: filename = "gtk.html";
		break;
		case 19: filename = "php.html";
		break;
		case 20: filename = "python.html";
		break;
		case 21: filename = "devcpp.html";
		break;
		case 22: filename = "devphp.html";
		break;
		case 23: filename = "dia.html";
		break;
		case 24: filename = "eclipse.html";
		break;
		case 25: filename = "notepad2.html";
		break;
		case 26: filename = "apache.html";
		break;
		case 27: filename = "mysql.html";
		break;
		case 28: filename = "tuxracer.html";
		break;
		case 29: filename = "winboard.html";
		break;

	}
	window.open("data/detailsfiles/"+filename,"Content","height=250,width=500,left=100,top=100");

}