This software is released for free public use under several open source licenses.
It is provided without any warranty, without even the implied warranty of merchantability or fitness for particular purpose. Use at your own risk.
Please see the license text included with each program for details.
Other parts of the CD are released under the GPL (Web pages) and their copyright is protected.

To obtain source code for each program please contact djihed@gmail.com